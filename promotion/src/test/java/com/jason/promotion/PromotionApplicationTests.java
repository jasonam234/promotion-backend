package com.jason.promotion;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jason.promotion.config.JwtTokenUtil;
import com.jason.promotion.promo.controller.PromoController;
import com.jason.promotion.promo.entity.ProductRecycler;
import com.jason.promotion.promo.entity.PromoEntity;
import com.jason.promotion.promo.repository.PromoCustomFields;
import com.jason.promotion.promo.repository.PromoRepository;
import com.jason.promotion.response.Response;
import com.jason.promotion.response.ResponseList;
import com.jason.promotion.response.ResponseMessage;
import com.jason.promotion.response.ResponseObject;
import com.jason.promotion.response.ResponseString;
import com.jason.promotion.user.controller.UserController;
import com.jason.promotion.user.entity.UserEntity;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
class PromotionApplicationTests {
	@Autowired
	ObjectMapper mapper;
	
	@Autowired
	JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	PromoController promoController;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@SuppressWarnings("deprecation")
	@Test
	public void givenWac_whenServletContext_thenItProvidesGreetController() {
	    ServletContext servletContext = webApplicationContext.getServletContext();
	    
	    Assert.assertNotNull(servletContext);
	    Assert.assertTrue(servletContext instanceof MockServletContext);
	    Assert.assertNotNull(webApplicationContext.getBean("promoController"));
	}
	
	public String generateToken() {
		UserEntity userOne = new UserEntity(1, "string", "string");
		String token = jwtTokenUtil.generateToken(userOne);
		
		return token;
	}
	
	public static String asJsonString(final Object object) {
		try {
			return new ObjectMapper().writeValueAsString(object);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void getAllPromotionsTest() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders
				.get("/api/promotion/get/promos")
				.contentType(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + generateToken())
				)
		.andExpect(status().isOk())
		.andReturn();
	}

	@Test
	public void getPromotionById() throws Exception{		
		mockMvc.perform(MockMvcRequestBuilders
				.get("/api/promotion/get/promos/1")
				.contentType(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + generateToken()))
		.andExpect(status().isOk())
		.andReturn();
	}

	@Test
	public void getPromotionByIdNoDataTest() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders
				.get("/api/promotion/get/promos/9999999")
				.contentType(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + generateToken()))
		.andExpect(status().isInternalServerError())
		.andReturn();
	}
	
	@Test
	public void postPromoSearchTest() throws Exception{
		UserEntity user = new UserEntity(1, "string", "string");
		PromoEntity data = new PromoEntity(1, user, "PROMO01", "Description", "2021/12/01", "2021/12/31"
				, 300000, "2021/12/31", "1");
		
		mockMvc.perform(MockMvcRequestBuilders
				.post("/api/promotion/post/search/promo")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(data))
				.header("Authorization", "Bearer " + generateToken()))
		.andExpect(status().isOk())
		.andReturn();
	}
	
	@Test
	public void deletePromoTest() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders
				.delete("/api/promotion/delete/promo/1")
				.contentType(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + generateToken()))
		.andExpect(status().isOk())
		.andReturn();
	}
	
	@Test
	public void searchAppliedPromoFreeGoodsTest() throws Exception{
		ArrayList<ProductRecycler> data = new ArrayList<ProductRecycler>();
	    data.add(new ProductRecycler("Sepatu", 3, 100000, 9));
	    data.add(new ProductRecycler("bbb", 3, 100000, 1));
	    data.add(new ProductRecycler("bbb", 3, 100000, 1));
		
		mockMvc.perform(MockMvcRequestBuilders
				.post("/api/promotion/post/applied_promos/free_goods")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(data))
				.header("Authorization", "Bearer " + generateToken()))
		.andExpect(status().isOk())
		.andReturn();
	}
	
	@Test
	public void searchAppliedPromoFreeGoodsTestFail() throws Exception{
		ArrayList<ProductRecycler> data = new ArrayList<ProductRecycler>();
		
		mockMvc.perform(MockMvcRequestBuilders
				.post("/api/promotion/post/applied_promos/free_goods")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(data))
				.header("Authorization", "Bearer " + generateToken()))
		.andExpect(status().isInternalServerError())
		.andReturn();
	}
	
	@Test
	public void searchAppliedPromoDiscountsTest() throws Exception{
		ArrayList<ProductRecycler> data = new ArrayList<ProductRecycler>();
	    data.add(new ProductRecycler("aaa", 3, 100000, 1));
	    data.add(new ProductRecycler("bbb", 3, 100000, 1));
	    data.add(new ProductRecycler("bbb", 3, 100000, 1));
		
		mockMvc.perform(MockMvcRequestBuilders
				.post("/api/promotion/post/applied_promos/discount")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(data))
				.header("Authorization", "Bearer " + generateToken()))
		.andExpect(status().isOk())
		.andReturn();
	}
	
	@Test
	public void searchAppliedPromoDiscountsTestFail() throws Exception{
		ArrayList<ProductRecycler> data = new ArrayList<ProductRecycler>();
		
		mockMvc.perform(MockMvcRequestBuilders
				.post("/api/promotion/post/applied_promos/discount")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(data))
				.header("Authorization", "Bearer " + generateToken()))
		.andExpect(content().string("{\"status\":500,\"message\":\"POST search applied promo terminated, promotion not found\"}"))
		.andReturn();
	}
}
