package com.jason.promotion;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import javax.servlet.ServletContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jason.promotion.config.JwtTokenUtil;
import com.jason.promotion.response.Response;
import com.jason.promotion.response.ResponseMessage;
import com.jason.promotion.response.ResponseObject;
import com.jason.promotion.response.ResponseString;
import com.jason.promotion.user.controller.UserController;
import com.jason.promotion.user.entity.UserEntity;

import junit.framework.Assert;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class UserControllerTest {
	@Autowired
	ObjectMapper mapper;
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	UserController userController;
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@BeforeEach
	public void setup() throws Exception {
	    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void givenWac_whenServletContext_thenItProvidesGreetController() {
	    ServletContext servletContext = webApplicationContext.getServletContext();
	    
	    Assert.assertNotNull(servletContext);
	    Assert.assertTrue(servletContext instanceof MockServletContext);
	    Assert.assertNotNull(webApplicationContext.getBean("userController"));
	}
	
	UserEntity userOne = new UserEntity(1, "string", "string");
	UserEntity userOneError = new UserEntity(1, "str", "str");
	UserEntity userOneErrorUsername = new UserEntity(1, "str", "string");
	UserEntity userOneErrorPassword = new UserEntity(1, "string", "str");
	
	public static String asJsonString(final Object object) {
		try {
			return new ObjectMapper().writeValueAsString(object);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Test
	public void test() {
		assertTrue(true);
	}

	@Test
	public void createAuthenticationTokenTest() throws Exception{
		mockMvc.perform(post("/api/authenticate")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(userOne)))
		.andExpect(status().isOk())
		.andReturn();

	}
	
//	@Test
//	public void createAuthenticationTokenTestWrongUsernameAndPassword() throws Exception{
//		Response response = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Login gagal, username atau password salah");
//		
////		mockMvc.perform(MockMvcRequestBuilders
////				.post("/api/authenticate")
////				.contentType(MediaType.APPLICATION_JSON)
////				.content(asJsonString(userOneError)))
////		.andExpect(result -> assertEquals(500
////				, ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response).getStatusCodeValue()))
////		.andReturn();
//		
//		mockMvc.perform(MockMvcRequestBuilders
//				.post("/api/authenticate")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(asJsonString(userOneError)))
//		.andExpect(status().isInternalServerError())
//		.andReturn();
//	}
//	
//	@Test
//	public void createAuthenticationTokenTestWrongUsername() throws Exception{
//		Response response = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Login gagal, username atau password salah");
//		
//		mockMvc.perform(post("/api/authenticate")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(asJsonString(userOneErrorUsername)))
//		.andExpect(result -> assertEquals(500
//				, ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response).getStatusCodeValue()));
//	}
//
//	@Test
//	public void createAuthenticationTokenTestWrongPassword() throws Exception{
//		Response response = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Login gagal, username atau password salah");
//		
//		mockMvc.perform(post("/api/authenticate")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(asJsonString(userOneErrorPassword)))
//		.andExpect(result -> assertEquals(500
//				, ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response).getStatusCodeValue()));
//	}
//
//	@Test
//	public void saveUserTest() throws Exception{
//		Response response = new ResponseObject(HttpStatus.OK.value(), "Pendaftaran pengguna berhasil", userOne);
//		
//		mockMvc.perform(post("/api/register")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(asJsonString(userOne)))
//		.andExpect(result -> assertEquals(200
//				, ResponseEntity.status(HttpStatus.OK.value()).body(response).getStatusCodeValue()));
//	}
//
//	@Test
//	public void saveUserUsernameNotAvailableTest() throws Exception{
//		Response response = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), "Username sudah terdaftar");
//		
//		mockMvc.perform(post("/api/register")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(asJsonString(userOne)))
//		.andExpect(result -> assertEquals(500
//				, ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response).getStatusCodeValue()))
//		.andReturn();
//	}
}
