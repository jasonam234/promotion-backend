package com.jason.promotion;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jason.promotion.config.JwtTokenUtil;
import com.jason.promotion.product.entity.ProductEntity;
import com.jason.promotion.promo.controller.PromoFreeGoodsController;
import com.jason.promotion.promo.entity.PromoDiscountEntity;
import com.jason.promotion.promo.entity.PromoEntity;
import com.jason.promotion.promo.entity.PromoFreeGoodsEntity;
import com.jason.promotion.user.entity.UserEntity;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class PromoFreeGoodsControllerTest {
	@Autowired
	ObjectMapper mapper;
	
	@Autowired
	JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@Autowired
	PromoFreeGoodsController promoFreeGoodsController;
	
	public String generateToken() {
		UserEntity userOne = new UserEntity(1, "string", "string");
		String token = jwtTokenUtil.generateToken(userOne);
		
		return token;
	}
	
	public static String asJsonString(final Object object) {
		try {
			return new ObjectMapper().writeValueAsString(object);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Test
	public void getFreeGoodsPromoByPromoIdTest() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders
				.get("/api/promotion/get/free_goods/1")
				.contentType(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + generateToken())
				)
		.andExpect(status().isOk())
		.andReturn();
	}
	
	@Test
	public void postNewFreeGoodsPromoTest() throws Exception{
		UserEntity userData = new UserEntity(1, "string", "string");
		PromoEntity promoData = new PromoEntity(1, userData, "PROMO01", "Description", "2021/12/01", "2021/12/31"
				, 300000, "2021/12/31", "1");
		ProductEntity productData = new ProductEntity(1, userData, "Item Name", 10000, 200000
				, "S-0001", "Description", "", "01/05/2019");
		PromoFreeGoodsEntity data = new PromoFreeGoodsEntity(1, productData, promoData, 1, "S-0001");
		
		mockMvc.perform(MockMvcRequestBuilders
				.post("/api/promotion/post/free_goods")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(data))
				.header("Authorization", "Bearer " + generateToken())
				)
		.andExpect(status().isOk())
		.andReturn();
	}
	
	@Test
	public void putFreeGoodsPromoTest() throws Exception {
		UserEntity userData = new UserEntity(1, "string", "string");
		PromoEntity promoData = new PromoEntity(1, userData, "PROMO01", "Description", "2021/12/01", "2021/12/31"
				, 300000, "2021/12/31", "1");
		ProductEntity productData = new ProductEntity(1, userData, "Item Name", 10000, 200000
				, "S-0001", "Description", "", "01/05/2019");
		PromoFreeGoodsEntity data = new PromoFreeGoodsEntity(1, productData, promoData, 1, "S-0001");
		
		mockMvc.perform(MockMvcRequestBuilders
				.put("/api/promotion/put/free_goods")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(data))
				.header("Authorization", "Bearer " + generateToken())
				)
		.andExpect(status().isOk())
		.andReturn();
	}
}
