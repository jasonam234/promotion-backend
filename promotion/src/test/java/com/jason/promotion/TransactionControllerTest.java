package com.jason.promotion;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.ServletContext;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jason.promotion.config.JwtTokenUtil;
import com.jason.promotion.promo.entity.PromoEntity;
import com.jason.promotion.transaction.controller.TransactionController;
import com.jason.promotion.transaction.entity.TransactionEntity;
import com.jason.promotion.user.entity.UserEntity;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class TransactionControllerTest {
	@Autowired
	ObjectMapper mapper;
	
	@Autowired
	JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@Autowired
	TransactionController transactionController;
	
	@SuppressWarnings("deprecation")
	@Test
	public void givenWac_whenServletContext_thenItProvidesGreetController() {
	    ServletContext servletContext = webApplicationContext.getServletContext();
	    
	    Assert.assertNotNull(servletContext);
	    Assert.assertTrue(servletContext instanceof MockServletContext);
	    Assert.assertNotNull(webApplicationContext.getBean("promoController"));
	}
	
	public String generateToken() {
		UserEntity userOne = new UserEntity(1, "string", "string");
		String token = jwtTokenUtil.generateToken(userOne);
		
		return token;
	}
	
	public static String asJsonString(final Object object) {
		try {
			return new ObjectMapper().writeValueAsString(object);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Test
	public void getAllTransactionTest() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders
				.get("/api/transaction/get/transactions")
				.contentType(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + generateToken())
				)
		.andExpect(status().isOk())
		.andReturn();
	}
	
	@Test
	public void getInvoiceByTransactionIdTest() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders
				.get("/api/transaction/get/invoice/1")
				.contentType(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + generateToken())
				)
		.andExpect(status().isOk())
		.andReturn();
	}
	
	@Test
	public void postSearchTransactionTest() throws Exception{
		UserEntity user = new UserEntity(1, "string", "string");
		PromoEntity promo = new PromoEntity(1, user, "PROMO01", "Description", "2021/12/01", "2021/12/31"
				, 300000, "2021/12/31", "1");
		TransactionEntity data = new TransactionEntity(1, user, promo, "INVOICE01", ""
				, "Description", "2021/12/01", 150000);
		
		mockMvc.perform(MockMvcRequestBuilders
				.post("/api/transaction/post/search/transaction")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(data))
				.header("Authorization", "Bearer " + generateToken())
				)
		.andExpect(status().isOk())
		.andReturn();
	}
	
	@Test
	public void postNewTransactionTest() throws Exception{
		UserEntity user = new UserEntity(1, "string", "string");
		PromoEntity promo = new PromoEntity(1, user, "PROMO01", "Description", "2021/12/01", "2021/12/31"
				, 300000, "2021/12/31", "1");
		TransactionEntity data = new TransactionEntity(1, user, promo, "INVOICE01", ""
				, "Description", "2021/12/01", 150000);
		
		mockMvc.perform(MockMvcRequestBuilders
				.post("/api/transaction/post/transaction")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(data))
				.header("Authorization", "Bearer " + generateToken())
				)
		.andExpect(status().isOk())
		.andReturn();
	}
	
	@Test
	public void postInvoiceByTransactionId() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders
				.post("/api/transaction/post/invoice/1")
				.contentType(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + generateToken())
				)
		.andExpect(status().isOk())
		.andReturn();
	}
}
