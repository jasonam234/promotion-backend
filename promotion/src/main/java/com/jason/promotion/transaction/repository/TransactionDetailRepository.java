package com.jason.promotion.transaction.repository;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.jason.promotion.transaction.entity.TransactionDetailEntity;

@Transactional
public interface TransactionDetailRepository extends JpaRepository<TransactionDetailEntity, Integer>{
	@Query(value = "select tr.transaction_detail_id, pr.name, tr.quantity, tr.price from transaction_detail tr "
			+ "join product pr on pr.product_id = tr.product_id "
			+ "where tr.transaction_id = ?", nativeQuery = true)
	List<TransactionDetailCustomFields.getTransactionDetailByTransactionId> 
		getTransactionDetailByTransactionId(int transaction_id);
		
	@Query(value = "select tr.transaction_detail_id, pr.name, tr.quantity, tr.price from transaction_detail tr "
			+ "join product pr on pr.product_id = tr.product_id "
			+ "order by tr.transaction_detail_id desc limit 1", nativeQuery = true)
	TransactionDetailCustomFields.getTransactionDetailByTransactionId getLatestTransactionDetail();
	
	@Query(value = "select p.product_code, p.name, tr.quantity, tr.price from transaction_detail tr "
			+ "join product p on p.product_id = tr.product_id "
			+ "where tr.transaction_id = ?", nativeQuery = true)
	ArrayList<TransactionDetailCustomFields.invoiceDetailTransactionData> getInvoiceTransactionDetailData(int transaction_id);
		
	@Modifying
	@Query(value = "insert into transaction_detail(transaction_id, product_id, quantity, price) values "
			+ "(?1, ?2, ?3, ?4)", nativeQuery = true)
	int postNewTransactionDetail(int transaction_id, int product_id, int quantity, int price);
}
