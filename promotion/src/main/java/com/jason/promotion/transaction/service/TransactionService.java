package com.jason.promotion.transaction.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.jason.promotion.promo.repository.PromoCustomFields;
import com.jason.promotion.response.Response;
import com.jason.promotion.response.ResponseList;
import com.jason.promotion.response.ResponseMessage;
import com.jason.promotion.response.ResponseObject;
import com.jason.promotion.transaction.entity.TransactionEntity;
import com.jason.promotion.transaction.repository.TransactionCustomFields;
import com.jason.promotion.transaction.repository.TransactionDetailCustomFields;
import com.jason.promotion.transaction.repository.TransactionDetailRepository;
import com.jason.promotion.transaction.repository.TransactionRepository;

@Service
public class TransactionService {
	@Autowired
	TransactionRepository transactionRepository;
	
	@Autowired
	TransactionDetailRepository transactionDetailRepository;
	
	@Autowired
	InvoiceGenerator invoiceGenerator;
	
	public ResponseEntity<Response> getAllTransaction(){
		List<TransactionCustomFields.getAllTransaction> data = transactionRepository.getAllTransaction();
		Response response = new ResponseList(HttpStatus.OK.value(), "GET all transaction data successful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> postSearchTransaction(TransactionEntity payload){
		List<TransactionCustomFields.getAllTransaction> data = 
				transactionRepository.postSearchTransaction(payload.getInvoice_number());
		Response response = new ResponseObject(HttpStatus.OK.value(), "POST search transaction data successful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> postNewTransaction(TransactionEntity payload){
		String faktur = "";
		TransactionCustomFields.getAllTransaction latestData = transactionRepository.getLatestTransaction();
		if(latestData == null) {
			faktur = "FAKTUR01";
		}else if(latestData.getTransaction_id() + 1 <= 9) {
			faktur = "FAKTUR0" + (latestData.getTransaction_id() + 1);
		}
		else if(latestData.getTransaction_id() + 1 > 9) {
			faktur = "FAKTUR" + (latestData.getTransaction_id() + 1);
		}
		
		if(payload.getPromotion().getPromotion_id() == 0) {
			transactionRepository.postNewTransaction(payload.getUser().getUser_id(), faktur, 
					payload.getDescription(), payload.getTotal_price());
			TransactionCustomFields.getAllTransaction data = transactionRepository.getLatestTransaction();
			Response response = new ResponseObject(HttpStatus.OK.value(), "POST transaction data successful", data);
			return ResponseEntity.status(HttpStatus.OK.value()).body(response);
		}else {
			transactionRepository.postNewTransactionPromotion(payload.getUser().getUser_id(), faktur, 
					payload.getDescription(), payload.getPromotion().getPromotion_id(), payload.getTotal_price());
			TransactionCustomFields.getAllTransaction data = transactionRepository.getLatestTransaction();
			Response response = new ResponseObject(HttpStatus.OK.value(), "POST transaction data successful", data);
			return ResponseEntity.status(HttpStatus.OK.value()).body(response);
		}
	}
	
	public ResponseEntity<Response> postInvoiceByTransactionId(int transaction_id){
		TransactionCustomFields.invoiceTransactionData transactionData = transactionRepository
				.getTransactionInvoiceData(transaction_id);
		if(transactionData == null) {
			Response response = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value()
					, "GET invoice terminated, transaction not found");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response);
		}else {
			ArrayList<TransactionDetailCustomFields.invoiceDetailTransactionData> transactionDetailData 
			= transactionDetailRepository.getInvoiceTransactionDetailData(transaction_id);
			
			invoiceGenerator.createInvoice(transactionData, transactionDetailData);
			
			Response response = new ResponseMessage(HttpStatus.OK.value()
					, "GET invoice successful");
			return ResponseEntity.status(HttpStatus.OK.value()).body(response);
		}
	}
	
	public ResponseEntity<InputStreamResource> getInvoiceByTransactionId(int transaction_id) throws IOException{
		var pdfFile = new ClassPathResource("invoice/" + transaction_id + ".pdf");
		return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
        		.body(new InputStreamResource(pdfFile.getInputStream()));
	}
}
