package com.jason.promotion.transaction.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.jason.promotion.promo.entity.PromoEntity;
import com.jason.promotion.user.entity.UserEntity;

@Entity(name = "transaction")
@Table(name = "transaction")
public class TransactionEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "transaction_id")
	int transaction_id;
	
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName= "user_id")
	UserEntity user;
	
	@ManyToOne
	@JoinColumn(name = "promotion_id", referencedColumnName= "promotion_id")
	PromoEntity promotion;
	
	String invoice_number;
	String payment_number;
	String description;
	String transaction_date;
	int total_price;
	
	public TransactionEntity() {  }

	public TransactionEntity(int transaction_id, UserEntity user, PromoEntity promotion, String invoice_number,
			String payment_number, String description, String transaction_date, int total_price) {
		super();
		this.transaction_id = transaction_id;
		this.user = user;
		this.promotion = promotion;
		this.invoice_number = invoice_number;
		this.payment_number = payment_number;
		this.description = description;
		this.transaction_date = transaction_date;
		this.total_price = total_price;
	}

	public int getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public PromoEntity getPromotion() {
		return promotion;
	}

	public void setPromotion(PromoEntity promotion) {
		this.promotion = promotion;
	}

	public String getInvoice_number() {
		return invoice_number;
	}

	public void setInvoice_number(String invoice_number) {
		this.invoice_number = invoice_number;
	}

	public String getPayment_number() {
		return payment_number;
	}

	public void setPayment_number(String payment_number) {
		this.payment_number = payment_number;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTransaction_date() {
		return transaction_date;
	}

	public void setTransaction_date(String transaction_date) {
		this.transaction_date = transaction_date;
	}

	public int getTotal_price() {
		return total_price;
	}

	public void setTotal_price(int total_price) {
		this.total_price = total_price;
	}

	@Override
	public String toString() {
		return "TransactionEntity [transaction_id=" + transaction_id + ", user=" + user + ", promotion=" + promotion
				+ ", invoice_number=" + invoice_number + ", payment_number=" + payment_number + ", description="
				+ description + ", transaction_date=" + transaction_date + ", total_price=" + total_price + "]";
	}
}
