package com.jason.promotion.transaction.service;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;

public class PdfPageEvent extends PdfPageEventHelper{
    public void onEndPage(PdfWriter writer,Document document) {
    	Font ordinary = FontFactory.getFont(FontFactory.COURIER);
		ordinary.setSize(5);  
		
		Paragraph paragraph = new Paragraph("page " + document.getPageNumber(), ordinary);
		paragraph.setAlignment(Paragraph.ALIGN_LEFT);
		
    	ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT
    			, paragraph, 290, 10, 0);   
    
    }
}
