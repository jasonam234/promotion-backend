package com.jason.promotion.transaction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jason.promotion.config.SecuredRestController;
import com.jason.promotion.response.Response;
import com.jason.promotion.transaction.entity.TransactionDetailEntity;
import com.jason.promotion.transaction.service.TransactionDetailService;

@RestController
public class TransactionDetailController implements SecuredRestController{
	@Autowired
	TransactionDetailService transactionDetailService;
	
	@GetMapping(value = "/api/transaction/get/transaction_details/{transaction_id}", produces = "application/json")
	public ResponseEntity<Response> getTransactionDetailByTransactionId(@PathVariable int transaction_id){
		return transactionDetailService.getTransactionDetailByTransactionId(transaction_id);
	}
	
	@PostMapping(value = "/api/transaction/post/transaction_detail", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> postTransactionDetail(@RequestBody TransactionDetailEntity payload){
		return transactionDetailService.postTransactionDetail(payload);
	}
}
