package com.jason.promotion.transaction.service;

import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.jason.promotion.promo.repository.PromoDiscountCustomFields;
import com.jason.promotion.promo.repository.PromoDiscountRepository;
import com.jason.promotion.promo.repository.PromoFreeGoodsCustomFields;
import com.jason.promotion.promo.repository.PromoFreeGoodsRepository;
import com.jason.promotion.promo.service.PromoDiscountService;
import com.jason.promotion.promo.service.PromoFreeGoodsService;
import com.jason.promotion.promo.service.PromoService;
import com.jason.promotion.response.Response;
import com.jason.promotion.response.ResponseObject;
import com.jason.promotion.transaction.repository.TransactionCustomFields;
import com.jason.promotion.transaction.repository.TransactionDetailCustomFields;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

@Service
public class InvoiceGenerator {
	
	@Autowired
	PromoDiscountRepository promoDiscountRepository;
	
	@Autowired
	PromoFreeGoodsRepository promoFreeGoodsRepository;
	
	public void createInvoice(TransactionCustomFields.invoiceTransactionData transaction
			, ArrayList<TransactionDetailCustomFields.invoiceDetailTransactionData> transactionDetail) {
		DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
		DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
		String transactionDate;
		String invoiceNumber;
		PromoDiscountCustomFields.getDiscountValue discountData = null;
		PromoFreeGoodsCustomFields.GetProductIdAndQuantity freeGoodsData = null;
		String promoDiscountProductCode = "";
		String promoFreeGoodsProductCode = "";
		int totalItemPrice;
		int subTotal = 0;
		int discount = 0;
		int finalTotal = 0;

		formatRp.setCurrencySymbol("Rp. ");
		formatRp.setMonetaryDecimalSeparator(',');
		formatRp.setGroupingSeparator('.');
		kursIndonesia.setDecimalFormatSymbols(formatRp);
		
		try {
			if(transaction.getPromotion_id() == 0) {
			}else {
				discountData = promoDiscountRepository.getDiscountByPromoId(transaction.getPromotion_id());
				freeGoodsData = promoFreeGoodsRepository.getFreeGoodsPromoByPromoId(transaction.getPromotion_id());
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		if(discountData != null) {
			promoDiscountProductCode = discountData.getProduct_code();
		}else if(freeGoodsData != null) {
			promoFreeGoodsProductCode = freeGoodsData.getProduct_code();
		}
		
		String path = "src/main/resources/invoice/" + transaction.getTransaction_id() + ".pdf";
		
		try {
			Font font = new Font(Font.HELVETICA, 12, Font.BOLDITALIC);
			Document doc = new Document(PageSize.A6, 10, 10, 10, 10);
			PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream(path));
			Font title = FontFactory.getFont(FontFactory.COURIER);
			title.setSize(18);    
			Font ordinary = FontFactory.getFont(FontFactory.COURIER);
			ordinary.setSize(5);    
			doc.open();
			
			Paragraph paragraph = new Paragraph("Invoice", title);
			paragraph.setAlignment(Paragraph.ALIGN_LEFT);
			doc.add(paragraph);
			
			transactionDate = "Date           : " + transaction.getTransaction_date(); 
			Paragraph date = new Paragraph(transactionDate, ordinary);
			date.setAlignment(Paragraph.ALIGN_LEFT);
			doc.add(date);
			
			invoiceNumber = "Invoice Number : " + transaction.getInvoice_number();
			Paragraph invoiceNum = new Paragraph(invoiceNumber, ordinary);
			invoiceNum.setAlignment(Paragraph.ALIGN_LEFT);
			doc.add(invoiceNum);
			
			PdfPTable table = new PdfPTable(5);
            table.setWidthPercentage(100);
            table.setSpacingBefore(10);
            table.setSpacingAfter(10);
            PdfPCell cell = new PdfPCell();
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPhrase(new Phrase("Product Code", ordinary));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Item Name", ordinary));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Price", ordinary));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Amount", ordinary));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Total Price", ordinary));
            table.addCell(cell);
            
            for(int i = 0; i < transactionDetail.size(); i++) {
            	totalItemPrice = 0;
            	cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                
                if(transactionDetail.get(i).getProduct_code().equals(promoFreeGoodsProductCode)){
                	table.deleteLastRow();
                	cell.setPhrase(new Phrase(transactionDetail.get(i-1).getProduct_code(), ordinary));
                    table.addCell(cell);
                	cell.setPhrase(new Phrase(transactionDetail.get(i-1).getName().concat("*"), ordinary));
                    table.addCell(cell);
                    cell.setPhrase(new Phrase(kursIndonesia.format(transactionDetail.get(i-1).getPrice()), ordinary));
                    table.addCell(cell);
                    cell.setPhrase(new Phrase(String.valueOf(transactionDetail.get(i-1).getQuantity()), ordinary));
                    table.addCell(cell);
                    totalItemPrice = transactionDetail.get(i-1).getPrice() * transactionDetail.get(i-1).getQuantity();
                    cell.setPhrase(new Phrase(kursIndonesia.format(totalItemPrice), ordinary));
                    table.addCell(cell);
                }
                
                cell.setPhrase(new Phrase(transactionDetail.get(i).getProduct_code(), ordinary));
                table.addCell(cell);
                
                if(transactionDetail.get(i).getProduct_code().equals(promoDiscountProductCode)) {
                	cell.setPhrase(new Phrase(transactionDetail.get(i).getName().concat("*"), ordinary));
                    table.addCell(cell);
                }else {
                	cell.setPhrase(new Phrase(transactionDetail.get(i).getName(), ordinary));
                    table.addCell(cell);
                }
                cell.setPhrase(new Phrase(kursIndonesia.format(transactionDetail.get(i).getPrice()), ordinary));
                table.addCell(cell);
                cell.setPhrase(new Phrase(String.valueOf(transactionDetail.get(i).getQuantity()), ordinary));
                table.addCell(cell);
                totalItemPrice = transactionDetail.get(i).getPrice() * transactionDetail.get(i).getQuantity();
                cell.setPhrase(new Phrase(kursIndonesia.format(totalItemPrice), ordinary));
                table.addCell(cell);
                subTotal = subTotal + totalItemPrice;
            }
            
            PdfPCell invincibleCell = new PdfPCell();
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPhrase(new Phrase("", ordinary));
            invincibleCell.setBorder(Rectangle.NO_BORDER);
            table.addCell(invincibleCell);
            invincibleCell.setPhrase(new Phrase("", ordinary));
            table.addCell(invincibleCell);
            invincibleCell.setPhrase(new Phrase("", ordinary));
            table.addCell(invincibleCell);
            cell.setPhrase(new Phrase("Subtotal", ordinary));
            table.addCell(cell);
            cell.setPhrase(new Phrase(kursIndonesia.format(subTotal), ordinary));
            table.addCell(cell);
            
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPhrase(new Phrase("", ordinary));
            invincibleCell.setBorder(Rectangle.NO_BORDER);
            table.addCell(invincibleCell);
            invincibleCell.setPhrase(new Phrase("", ordinary));
            table.addCell(invincibleCell);
            invincibleCell.setPhrase(new Phrase("", ordinary));
            table.addCell(invincibleCell);
            cell.setPhrase(new Phrase("Discount", ordinary));
            table.addCell(cell);
            cell.setPhrase(new Phrase(kursIndonesia.format(transaction.getTotal_price() - subTotal), ordinary));
            table.addCell(cell);
            
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setPhrase(new Phrase("", ordinary));
            invincibleCell.setBorder(Rectangle.NO_BORDER);
            table.addCell(invincibleCell);
            invincibleCell.setPhrase(new Phrase("", ordinary));
            table.addCell(invincibleCell);
            invincibleCell.setPhrase(new Phrase("", ordinary));
            table.addCell(invincibleCell);
            cell.setPhrase(new Phrase("Total", ordinary));
            table.addCell(cell);
            cell.setPhrase(new Phrase(kursIndonesia.format(transaction.getTotal_price()), ordinary));
            table.addCell(cell);
            
            doc.add(table);
            
            PdfPageEvent event = new PdfPageEvent();
            writer.setPageEvent(event);
            
			doc.close();
			writer.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
