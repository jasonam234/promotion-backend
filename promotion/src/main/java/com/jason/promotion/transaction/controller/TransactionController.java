package com.jason.promotion.transaction.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jason.promotion.config.SecuredRestController;
import com.jason.promotion.response.Response;
import com.jason.promotion.transaction.entity.TransactionEntity;
import com.jason.promotion.transaction.repository.TransactionRepository;
import com.jason.promotion.transaction.service.TransactionService;

@RestController
public class TransactionController implements SecuredRestController{
	@Autowired
	TransactionService transactionService;
	
	@GetMapping(value = "/api/transaction/get/transactions", produces = "application/json")
	public ResponseEntity<Response> getAllTransaction(){
		return transactionService.getAllTransaction();
	}
	
	@GetMapping(value = "/api/transaction/get/invoice/{transaction_id}", produces = "application/json")
	public ResponseEntity<InputStreamResource> getInvoiceByTransactionId(@PathVariable int transaction_id)  throws IOException{
		return transactionService.getInvoiceByTransactionId(transaction_id);
	}
	
	@PostMapping(value = "/api/transaction/post/search/transaction", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> postSearchTransaction(@RequestBody TransactionEntity payload){
		return transactionService.postSearchTransaction(payload);
	}
	
	@PostMapping(value = "/api/transaction/post/transaction", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> postNewTransaction(@RequestBody TransactionEntity payload){
		return transactionService.postNewTransaction(payload);
	}
	
	@PostMapping(value = "/api/transaction/post/invoice/{transaction_id}", produces = "application/json")
	public ResponseEntity<Response> postInvoiceByTransactionId(@PathVariable int transaction_id){
		return transactionService.postInvoiceByTransactionId(transaction_id);
	}
}