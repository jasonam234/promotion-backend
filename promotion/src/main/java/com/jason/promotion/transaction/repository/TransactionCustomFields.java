package com.jason.promotion.transaction.repository;

public interface TransactionCustomFields {
	public interface getAllTransaction{
		public int getTransaction_id();
		public String getInvoice_number();
		public String getTransaction_date();
		public int getTotal_price();
		public String getDescription();
	}
	
	public interface invoiceTransactionData{
		public int getTransaction_id();
		public int getPromotion_id();
		public String getInvoice_number();
		public String getDescription();
		public String getTransaction_date();
		public int getTotal_price();
	}
}
