package com.jason.promotion.transaction.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jason.promotion.promo.entity.PromoEntity;
import com.jason.promotion.promo.repository.PromoCustomFields;
import com.jason.promotion.transaction.entity.TransactionEntity;

@Transactional
public interface TransactionRepository extends JpaRepository<TransactionEntity, Integer>{
	@Query(value = "select transaction_id, invoice_number, description, transaction_date, total_price, last_edit "
			+ "from transaction", nativeQuery = true)
	List<TransactionCustomFields.getAllTransaction> getAllTransaction();
	
	@Query(value = "select transaction_id, invoice_number, description, transaction_date, total_price, last_edit "
			+ "from transaction order by transaction_id desc limit 1 ", nativeQuery = true)
	TransactionCustomFields.getAllTransaction getLatestTransaction();
	
	@Query(value = "select transaction_id, promotion_id, invoice_number, description, transaction_date, total_price "
			+ "from transaction where transaction_id = ?", nativeQuery = true)
	TransactionCustomFields.invoiceTransactionData getTransactionInvoiceData(int transaction_id);
	
	@Modifying
	@Query(value = "select transaction_id, invoice_number, description, transaction_date, total_price"
			+ ", last_edit from transaction "
			+ "where invoice_number like %:invoice_number%", nativeQuery = true)
	List<TransactionCustomFields.getAllTransaction> postSearchTransaction(@Param("invoice_number") String invoice_number);
	
	@Modifying
	@Query(value = "insert into transaction(user_id, invoice_number, description, "
			+ "transaction_date, total_price, last_edit, deleted) values "
			+ "(?1, ?2, ?3, now(), ?4, now(), 0)", nativeQuery = true)
	int postNewTransaction(int user_id, String invoice_number, String description, int total_price);
	
	@Modifying
	@Query(value = "insert into transaction(user_id, invoice_number, description, promotion_id, "
			+ "transaction_date, total_price, last_edit, deleted) values "
			+ "(?1, ?2, ?3, ?4, now(), ?5, now(), 0)", nativeQuery = true)
	int postNewTransactionPromotion(int user_id, String invoice_number, String description, int promotion_id, int total_price);
}
