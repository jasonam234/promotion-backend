package com.jason.promotion.transaction.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.jason.promotion.product.repository.ProductCustomFields;
import com.jason.promotion.product.repository.ProductRepository;
import com.jason.promotion.response.Response;
import com.jason.promotion.response.ResponseList;
import com.jason.promotion.response.ResponseMessage;
import com.jason.promotion.response.ResponseObject;
import com.jason.promotion.transaction.entity.TransactionDetailEntity;
import com.jason.promotion.transaction.repository.TransactionCustomFields;
import com.jason.promotion.transaction.repository.TransactionDetailCustomFields;
import com.jason.promotion.transaction.repository.TransactionDetailRepository;

@Service
public class TransactionDetailService {
	@Autowired
	TransactionDetailRepository transactionDetailRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	public ResponseEntity<Response> getTransactionDetailByTransactionId(int transaction_id){
		List<TransactionDetailCustomFields.getTransactionDetailByTransactionId> data = 
				transactionDetailRepository.getTransactionDetailByTransactionId(transaction_id);
		Response response = new ResponseList(HttpStatus.OK.value(), "GET all transaction detail data successful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> postTransactionDetail(TransactionDetailEntity payload){
		ProductCustomFields.getAllProductDataById productData = 
				productRepository.getProductByProductId(payload.getProduct().getProduct_id());
		
		if(productData == null) {
			Response response = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), 
					"POST Transaction detail terminated, product not found");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response);
		}
		
		transactionDetailRepository.postNewTransactionDetail(payload.getTransaction().getTransaction_id(), 
				payload.getProduct().getProduct_id(), payload.getQuantity(), payload.getPrice());
		productRepository.updateProductStock(payload.getQuantity(), payload.getProduct().getProduct_id());
		
		TransactionDetailCustomFields.getTransactionDetailByTransactionId data = 
				transactionDetailRepository.getLatestTransactionDetail();
		Response response = new ResponseObject(HttpStatus.OK.value(), "GET all transaction detail data successful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
}
