package com.jason.promotion.transaction.entity;

import javax.persistence.*;

import com.jason.promotion.product.entity.ProductEntity;
import com.jason.promotion.user.entity.UserEntity;

@Entity(name = "transaction_detail")
@Table(name = "transaction_detail")
public class TransactionDetailEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "transaction_detail_id")
	int transaction_detail_id;
	
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName= "user_id")
	TransactionEntity transaction;
	
	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName= "product_id")
	ProductEntity product;
	
	int quantity;
	int price;
	
	public TransactionDetailEntity() {  }

	public TransactionDetailEntity(int transaction_detail_id, TransactionEntity transaction, ProductEntity product,
			int quantity, int price) {
		super();
		this.transaction_detail_id = transaction_detail_id;
		this.transaction = transaction;
		this.product = product;
		this.quantity = quantity;
		this.price = price;
	}

	public int getTransaction_detail_id() {
		return transaction_detail_id;
	}

	public void setTransaction_detail_id(int transaction_detail_id) {
		this.transaction_detail_id = transaction_detail_id;
	}

	public TransactionEntity getTransaction() {
		return transaction;
	}

	public void setTransaction(TransactionEntity transaction) {
		this.transaction = transaction;
	}

	public ProductEntity getProduct() {
		return product;
	}

	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "TransactionDetailEntity [transaction_detail_id=" + transaction_detail_id + ", transaction="
				+ transaction + ", product=" + product + ", quantity=" + quantity + ", price=" + price + "]";
	}
}
