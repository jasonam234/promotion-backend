package com.jason.promotion.transaction.repository;

public interface TransactionDetailCustomFields {
	public interface getTransactionDetailByTransactionId{
		public int getTransaction_detail_id();
		public String getName();
		public int getQuantity();
		public int getPrice();
	}
	
	public interface invoiceDetailTransactionData{
		public String getProduct_code();
		public String getName();
		public int getQuantity();
		public int getPrice();
	}
}
