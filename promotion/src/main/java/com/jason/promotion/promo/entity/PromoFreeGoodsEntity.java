package com.jason.promotion.promo.entity;

import javax.persistence.*;

import com.jason.promotion.product.entity.ProductEntity;

@Entity(name = "promotion_free_goods")
@Table(name = "promotion_free_goods")
public class PromoFreeGoodsEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "promotion_free_goods_id")
	int promotion_free_goods_id;
	
	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName= "product_id")
	ProductEntity product;
	
	@ManyToOne
	@JoinColumn(name = "promotion_id", referencedColumnName= "promotion_id")
	PromoEntity promo;
	
	int quantity;
	String product_code;
	
	public PromoFreeGoodsEntity() { }

	public PromoFreeGoodsEntity(int promotion_free_goods_id, ProductEntity product, PromoEntity promo, int quantity,
			String product_code) {
		super();
		this.promotion_free_goods_id = promotion_free_goods_id;
		this.product = product;
		this.promo = promo;
		this.quantity = quantity;
		this.product_code = product_code;
	}

	public int getPromotion_free_goods_id() {
		return promotion_free_goods_id;
	}

	public void setPromotion_free_goods_id(int promotion_free_goods_id) {
		this.promotion_free_goods_id = promotion_free_goods_id;
	}

	public ProductEntity getProduct() {
		return product;
	}

	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	public PromoEntity getPromo() {
		return promo;
	}

	public void setPromo(PromoEntity promo) {
		this.promo = promo;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	@Override
	public String toString() {
		return "PromoFreeGoodsEntity [promotion_free_goods_id=" + promotion_free_goods_id + ", product=" + product
				+ ", promo=" + promo + ", quantity=" + quantity + ", product_code=" + product_code + "]";
	}

	
}
