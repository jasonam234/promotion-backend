package com.jason.promotion.promo.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.jason.promotion.promo.entity.ItemTermEntity;

@Transactional
public interface ItemTermRepository extends JpaRepository<ItemTermEntity, Integer>{
	@Query(value = "select term_id, promotion_id, product_id from item_term order by term_id desc limit 1", nativeQuery = true)
	ItemTermCustomFields.getAllItemTermData getLastTerm();
	
	@Query(value = "select it.term_id, it.product_id, pr.product_code, pr.name from item_term it "
			+ "join product pr on pr.product_id = it.product_id "
			+ "where it.promotion_id = ?", nativeQuery = true)
	List<ItemTermCustomFields.getItemTermByPromoId> getItemTermByPromoId(int promotion_id);
	
	@Query(value = "select it.term_id, it.product_id, pr.product_code, pr.name from item_term it "
			+ "join product pr on pr.product_id = it.product_id "
			+ "where it.term_id = ?", nativeQuery = true)
	ItemTermCustomFields.getItemTermByPromoId getItemTermById(int term_id);
	
	@Modifying
	@Query(value = "insert into item_term(promotion_id, product_id) value(?1, ?2)", nativeQuery = true)
	int postNewItemTerm(int promotion_id, int product_id);
	
	@Modifying
	@Query(value = "update item_term set product_id = ?1 where term_id = ?2", nativeQuery = true)
	int putItemTerm(int product_id, int term_id);
}
