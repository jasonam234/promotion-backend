package com.jason.promotion.promo.entity;

import javax.persistence.*;

import com.jason.promotion.user.entity.UserEntity;

@Entity(name = "promotion")
@Table(name = "promotion")
public class PromoEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "promotion_id")
	int promotion_id;
	
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName= "user_id")
	UserEntity user;
	
	String document_number;
	String description;
	String start_date;
	String end_date;
	int promo_value;
	String last_edit;
	
	String product_id;
	
	public PromoEntity() { }

	public PromoEntity(int promotion_id, UserEntity user, String document_number, String description, String start_date,
			String end_date, int promo_value, String last_edit, String product_id) {
		super();
		this.promotion_id = promotion_id;
		this.user = user;
		this.document_number = document_number;
		this.description = description;
		this.start_date = start_date;
		this.end_date = end_date;
		this.promo_value = promo_value;
		this.last_edit = last_edit;
		this.product_id = product_id;
	}

	public int getPromotion_id() {
		return promotion_id;
	}

	public void setPromotion_id(int promotion_id) {
		this.promotion_id = promotion_id;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getDocument_number() {
		return document_number;
	}

	public void setDocument_number(String document_number) {
		this.document_number = document_number;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public int getPromo_value() {
		return promo_value;
	}

	public void setPromo_value(int promo_value) {
		this.promo_value = promo_value;
	}

	public String getLast_edit() {
		return last_edit;
	}

	public void setLast_edit(String last_edit) {
		this.last_edit = last_edit;
	}

	public String getProduct_id() {
		return product_id;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	@Override
	public String toString() {
		return "PromoEntity [promotion_id=" + promotion_id + ", user=" + user + ", document_number=" + document_number
				+ ", description=" + description + ", start_date=" + start_date + ", end_date=" + end_date
				+ ", promo_value=" + promo_value + ", last_edit=" + last_edit + ", product_id=" + product_id + "]";
	}
}
