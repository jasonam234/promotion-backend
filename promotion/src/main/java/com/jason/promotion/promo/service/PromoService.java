package com.jason.promotion.promo.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.jason.promotion.product.entity.ProductEntity;
import com.jason.promotion.promo.entity.ProductRecycler;
import com.jason.promotion.promo.entity.PromoDiscountEntity;
import com.jason.promotion.promo.entity.PromoEntity;
import com.jason.promotion.promo.repository.PromoCustomFields;
import com.jason.promotion.promo.repository.PromoDiscountCustomFields;
import com.jason.promotion.promo.repository.PromoDiscountRepository;
import com.jason.promotion.promo.repository.PromoFreeGoodsCustomFields;
import com.jason.promotion.promo.repository.PromoFreeGoodsRepository;
import com.jason.promotion.promo.repository.PromoRepository;
import com.jason.promotion.response.Response;
import com.jason.promotion.response.ResponseList;
import com.jason.promotion.response.ResponseMessage;
import com.jason.promotion.response.ResponseObject;

@Service
public class PromoService {
	@Autowired
	PromoRepository promoRepository;
	
	@Autowired
	PromoFreeGoodsRepository promoFreeGoodsRepository;
	
	@Autowired
	PromoDiscountRepository promoDiscountRepository;
	
	public ResponseEntity<Response> getAllPromo(){
		List<PromoCustomFields.getAllPromoData> data = promoRepository.getAllPromo();
		Response response = new ResponseList(HttpStatus.OK.value(), "GET all promo data succesful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> getPromoById(int promotionId){
		PromoCustomFields.getAllPromoData data = promoRepository.getPromoById(promotionId);
		
		if(data == null) {
			Response response = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), 
					"GET promo data terminated, no data found");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response);
		}
		
		Response response = new ResponseObject(HttpStatus.OK.value(), "GET promo data succesful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> postPromoSearch(PromoEntity payload){
		List<PromoCustomFields.getAllPromoData> data = promoRepository.postSearchPromo(payload.getDocument_number(), 
				payload.getProduct_id());
		Response response = new ResponseList(HttpStatus.OK.value(), "POST promo search success", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
		
	}
	
	public ResponseEntity<Response> getAllDocumentNumber(){
		List<PromoCustomFields.getDocumentNumber> data = promoRepository.getDocumentNumber();
		Response response = new ResponseList(HttpStatus.OK.value(), "GET all document Number succesful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> getAllProductId(){
		List<PromoCustomFields.getProductId> data = promoRepository.getProductId();
		Response response = new ResponseList(HttpStatus.OK.value(), "GET all product ID succesful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> deletePromo(int promotion_id){
		promoRepository.deletePromo(promotion_id);
		PromoCustomFields.getAllPromoData data = promoRepository.getPromoById(promotion_id);
		
		if(data == null) {
			Response response = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), 
					"GET promo data terminated, no data found");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response);
		}
		
		Response response = new ResponseObject(HttpStatus.OK.value(), "DELETE promo data succesful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> searchAppliedPromoFreeGoods(ArrayList<ProductRecycler> payload){
		Set<Integer> productId = new HashSet<Integer>();
		int totalPrice = 0;
		for(int i = 0; i < payload.size(); i++) {
			productId.add(payload.get(i).getProductId());
		}
		PromoFreeGoodsCustomFields.CheckFreeGoodsPromo freeGoodsData = promoFreeGoodsRepository.checkFreeGoodsPromo(productId);
		
		if(freeGoodsData == null) {
			Response responseFreeGoods = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value()
					, "POST search applied promo terminated, promotion not found");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(responseFreeGoods);
		}else {
			for(int i = 0; i < payload.size(); i++) {
				if(payload.get(i).getProductId() == freeGoodsData.getProduct_id()) {
					totalPrice = payload.get(i).getPrice() * payload.get(i).getQuantity();
					if(totalPrice > Integer.valueOf(freeGoodsData.getPromo_value())) {
						PromoFreeGoodsCustomFields.GetProductIdAndQuantity data = 
								promoFreeGoodsRepository.getFreeGoodsPromoByPromoId(freeGoodsData.getPromotion_id());
						Response response = new ResponseObject(HttpStatus.OK.value()
								, "POST search applied promo successful", data);
						return ResponseEntity.status(HttpStatus.OK.value()).body(response);
					}else {
						Response responseFreeGoods = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value()
								, "POST search applied promo terminated, promotion not found");
						return ResponseEntity.status(HttpStatus.OK.value()).body(responseFreeGoods);
					}
				}
			}
			Response responseFreeGoods = new ResponseObject(HttpStatus.OK.value()
					, "POST search applied promo successful", freeGoodsData);
			return ResponseEntity.status(HttpStatus.OK.value()).body(responseFreeGoods);
		}
	}
	
	public ResponseEntity<Response> searchAppliedPromoDiscount(ArrayList<ProductRecycler> payload){
		Set<Integer> productId = new HashSet<Integer>();
		int totalPrice = 0;
		for(int i = 0; i < payload.size(); i++) {
			productId.add(payload.get(i).getProductId());
		}
		PromoDiscountCustomFields.checkDiscountPromo discountData = promoDiscountRepository.checkDiscountPromo(productId);
		if(discountData == null) {
			Response responseDiscount = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value()
					, "POST search applied promo terminated, promotion not found");
			return ResponseEntity.status(HttpStatus.OK.value()).body(responseDiscount);
		}else {
			for(int i = 0; i < payload.size(); i++) {
				if(payload.get(i).getProductId() == discountData.getProduct_id()) {
					totalPrice = payload.get(i).getPrice() * payload.get(i).getQuantity();
					if(totalPrice > Integer.valueOf(discountData.getPromo_value())) {
						Response responseDiscount = new ResponseObject(HttpStatus.OK.value()
								, "POST search applied promo successful", discountData);
						return ResponseEntity.status(HttpStatus.OK.value()).body(responseDiscount);
					}else {
						Response responseDiscount = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value()
								, "POST search applied promo terminated, promotion not found");
						return ResponseEntity.status(HttpStatus.OK.value()).body(responseDiscount);
					}
				}
			}
		}
		Response responseDiscount = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value()
				, "POST search applied promo terminated, promotion not found");
		return ResponseEntity.status(HttpStatus.OK.value()).body(responseDiscount);
	}
}
