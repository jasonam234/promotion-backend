package com.jason.promotion.promo.repository;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jason.promotion.promo.entity.PromoDiscountEntity;
import com.jason.promotion.user.entity.UserEntity;

@Transactional
public interface PromoDiscountRepository extends JpaRepository<PromoDiscountEntity, Integer>{
	@Query(value = "select promo_disc_id, promotion_id, discount_value from promo_discount", nativeQuery = true)
	List<PromoDiscountCustomFields.getAllPromoDiscountData> getAllPromoDiscount();
	
	@Query(value = "select promo_disc_id, promotion_id, discount_value from promo_discount", nativeQuery = true)
	List<PromoDiscountEntity> getAllPromoDiscountDev();
	
	@Query(value = "select pd.promo_disc_id, pd.discount_value, pr.product_code from promo_discount pd "
			+ "join item_term it on it.promotion_id = pd.promotion_id "
			+ "join product pr on pr.product_id = it.product_id "
			+ "where pd.promotion_id = ?", nativeQuery = true)
	PromoDiscountCustomFields.getDiscountValue getDiscountByPromoId(int promotion_id);
	
	@Query(value = "select pd.promotion_id, pd.discount_value, p.document_number"
			+ ", p.description, p.start_date, p.end_date, p.promo_value, p.last_edit from promo_discount pd "
			+ "join promotion p on p.promotion_id = pd.promotion_id "
			+ "where pd.promotion_id = ?", nativeQuery = true)
	List<PromoDiscountCustomFields.getLatestPromo> getDiscountByPromoIdComplete(int promotion_id);
	
	@Query(value = "select pd.promotion_id, pd.discount_value, p.document_number"
			+ ", p.description, p.start_date, p.end_date, p.promo_value, p.last_edit from promo_discount pd "
			+ "join promotion p on p.promotion_id = pd.promotion_id "
			+ "order by p.promotion_id desc limit 1", nativeQuery = true)
	PromoDiscountCustomFields.getLatestPromo getLastPromoDiscount();
	
	@Query(value = "select pr.promotion_id, p.price, d.discount_value, it.product_id, pr.promo_value from promotion pr "
			+ "join promo_discount d on d.promotion_id = pr.promotion_id "
			+ "join item_term it on it.promotion_id = pr.promotion_id "
			+ "join product p on p.product_id = it.product_id "
			+ "where now() > pr.start_date and now() < pr.end_date "
			+ "and d.discount_value =  "
			+ "(SELECT MAX(discount_value) FROM promo_discount pd "
			+ "join promotion pr on pr.promotion_id = pd.promotion_id "
			+ "join item_term it on it.promotion_id = pr.promotion_id "
			+ "where pr.deleted = 0 "
			+ "and it.product_id in :product_id) "
			+ "and it.product_id in :product_id "
			+ "and pr.deleted = 0 "
			+ "ORDER BY UNIX_TIMESTAMP(pr.start_date) asc limit 1", nativeQuery = true)
	PromoDiscountCustomFields.checkDiscountPromo checkDiscountPromo(@Param("product_id") Set<Integer> product_id);
	
	@Modifying
	@Query(value = "insert into promo_discount(promotion_id, discount_value) value(?1, ?2)", nativeQuery = true)
	int postNewPromoDiscount(int promotion_id, int discount_value);
	
	@Modifying
	@Query(value = "update promo_discount set discount_value = ?1 where promotion_id = ?2", nativeQuery = true)
	int putPromoDiscount(int discount_value, int promotion_id);
}