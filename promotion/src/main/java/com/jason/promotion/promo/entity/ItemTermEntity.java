package com.jason.promotion.promo.entity;

import javax.persistence.*;

import com.jason.promotion.product.entity.ProductEntity;

@Entity(name = "item_term")
@Table(name = "item_term")
public class ItemTermEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "term_id")
	int term_id;
	
	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName= "product_id")
	ProductEntity product;
	
	@ManyToOne
	@JoinColumn(name = "promotion_id", referencedColumnName= "promotion_id")
	PromoEntity promo;
	
	public ItemTermEntity() { }

	public ItemTermEntity(int term_id, ProductEntity product, PromoEntity promo) {
		super();
		this.term_id = term_id;
		this.product = product;
		this.promo = promo;
	}

	public int getTerm_id() {
		return term_id;
	}

	public void setTerm_id(int term_id) {
		this.term_id = term_id;
	}

	public ProductEntity getProduct() {
		return product;
	}

	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	public PromoEntity getPromo() {
		return promo;
	}

	public void setPromo(PromoEntity promo) {
		this.promo = promo;
	}

	@Override
	public String toString() {
		return "ItemTermEntity [term_id=" + term_id + ", product=" + product + ", promo=" + promo + "]";
	}
}
