package com.jason.promotion.promo.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jason.promotion.product.entity.ProductEntity;
import com.jason.promotion.promo.entity.ProductRecycler;
import com.jason.promotion.promo.entity.PromoFreeGoodsEntity;

@Transactional
public interface PromoFreeGoodsRepository extends JpaRepository<PromoFreeGoodsEntity, Integer>{
	@Query(value = "select fg.promotion_id, fg.promotion_free_goods_id, p.document_number, p.description, p.start_date, "
			+ "p.end_date, p.promo_value, pr.name, pr.product_code, p.last_edit from promotion_free_goods fg "
			+ "join promotion p on p.promotion_id = fg.promotion_id "
			+ "join product pr on pr.product_id = fg.product_id "
			+ "order by p.promotion_id desc limit 1;", nativeQuery = true)
	PromoFreeGoodsCustomFields.PostNewPromoFreeGoodsData getLatestPromoFreeGoods();
	
	@Query(value = "select fg.promotion_free_goods_id, fg.promotion_id, pr.product_id, pr.name, fg.quantity, pr.product_code from promotion_free_goods fg "
			+ "join product pr on pr.product_id = fg.product_id "
			+ "where fg.promotion_id = ?", nativeQuery = true)
	PromoFreeGoodsCustomFields.GetProductIdAndQuantity getFreeGoodsPromoByPromoId(int promotion_id);
	
	@Query(value = "select pr.promotion_id, fg.quantity , it.product_id, fg.product_id as term, pr.promo_value from promotion pr "
			+ "join promotion_free_goods fg on fg.promotion_id = pr.promotion_id "
			+ "join item_term it on it.promotion_id = pr.promotion_id "
			+ "join product p on fg.product_id = p.product_id "
			+ "where now() > pr.start_date and now() < pr.end_date "
			+ "and fg.quantity * p.price = (SELECT MAX(fg.quantity * p.price) "
			+ "FROM promotion_free_goods fg "
			+ "join product p on fg.product_id = p.product_id "
			+ "join promotion pr on fg.promotion_id = pr.promotion_id "
			+ "join item_term it on it.promotion_id = pr.promotion_id "
			+ "and it.product_id in :product_id "
			+ "where pr.deleted = 0) "
			+ "and it.product_id in :product_id "
			+ "and pr.deleted = 0 "
			+ "ORDER BY UNIX_TIMESTAMP(pr.start_date) asc limit 1", nativeQuery = true)
	PromoFreeGoodsCustomFields.CheckFreeGoodsPromo checkFreeGoodsPromo(@Param("product_id") Set<Integer> product_id);
	
	@Modifying
	@Query(value = "insert into promotion_free_goods(promotion_id, product_id, quantity) "
			+ "value(?1, ?2, ?3)", nativeQuery = true)
	int postNewPromoFreeGoods(int promotion_id, int product_id, int quantity);
	
	@Modifying
	@Query(value = "update promotion_free_goods set product_id = ?1, quantity = ?2 where promotion_id = ?3", nativeQuery = true)
	int putPromoFreeGoods(int product_id, int quantity, int promotion_id);
}
