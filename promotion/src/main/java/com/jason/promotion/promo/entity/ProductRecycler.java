package com.jason.promotion.promo.entity;

public class ProductRecycler {
	private String name;
    private int quantity;
    private int price;
    private int productId;

    public ProductRecycler() {}

	public ProductRecycler(String name, int quantity, int price, int productId) {
		super();
		this.name = name;
		this.quantity = quantity;
		this.price = price;
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "ProductRecycler [name=" + name + ", quantity=" + quantity + ", price=" + price + ", productId="
				+ productId + "]";
	}

}
