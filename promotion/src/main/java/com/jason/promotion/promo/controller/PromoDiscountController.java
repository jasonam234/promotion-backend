package com.jason.promotion.promo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jason.promotion.config.SecuredRestController;
import com.jason.promotion.promo.entity.PromoDiscountEntity;
import com.jason.promotion.promo.repository.PromoDiscountCustomFields;
import com.jason.promotion.promo.repository.PromoDiscountRepository;
import com.jason.promotion.promo.repository.PromoRepository;
import com.jason.promotion.promo.service.PromoDiscountService;
import com.jason.promotion.response.Response;
import com.jason.promotion.response.ResponseObject;

@RestController
public class PromoDiscountController implements SecuredRestController{
	@Autowired 
	PromoDiscountService promoDiscountService;
	
	@GetMapping(value = "/api/promotion/get/discounts", produces = "application/json")
	public ResponseEntity<Response> getAllDiscountPromo(){
		return promoDiscountService.getAllPromoDiscount();
	}
	
	@GetMapping(value = "/api/promotion/get/discount/{promotion_id}", produces = "application/json")
	public ResponseEntity<Response> getDiscountPromoByPromoId(@PathVariable int promotion_id){
		return promoDiscountService.getDiscountPromoByPromoId(promotion_id);
	}
	
	@PostMapping(value = "/api/promotion/post/discount", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> postNewDiscountPromo(@RequestBody PromoDiscountEntity payload){
		return promoDiscountService.postNewDiscountPromo(payload);
	}
	
	@PutMapping(value = "/api/promotion/put/discount", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> putDiscountPromo(@RequestBody PromoDiscountEntity payload){
		return promoDiscountService.putDiscountPromo(payload);
	}
}
