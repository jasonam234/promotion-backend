package com.jason.promotion.promo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jason.promotion.config.SecuredRestController;
import com.jason.promotion.promo.entity.ItemTermEntity;
import com.jason.promotion.promo.service.ItemTermService;
import com.jason.promotion.response.Response;

@RestController
public class ItemTermController implements SecuredRestController {
	@Autowired
	ItemTermService itemTermService;
	
	@PostMapping(value = "/api/promotion/post/item_term")
	public ResponseEntity<Response> postNewTerm(@RequestBody ItemTermEntity payload){
		return itemTermService.postNewTerm(payload);
	}
	
	@GetMapping(value = "/api/promotion/get/item_term/{promotion_id}")
	public ResponseEntity<Response> getItemTermByPromoId(@PathVariable int promotion_id){
		return itemTermService.getItemTermByPromoId(promotion_id);
	}
	
	@PutMapping(value = "/api/promotion/put/item_term", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> putItemTerm(@RequestBody ItemTermEntity payload){
		return itemTermService.putItemTerm(payload);
	}
}
