package com.jason.promotion.promo.entity;

import javax.persistence.*;

@Entity(name = "promo_discount")
@Table(name = "promo_discount")
public class PromoDiscountEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "promo_disc_id")
	int promo_disc_id;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "promotion_id", referencedColumnName= "promotion_id")
	PromoEntity promo;
	
	int discount_value;
	
	public PromoDiscountEntity() { }

	public PromoDiscountEntity(int promo_disc_id, PromoEntity promo, int discount_value) {
		super();
		this.promo_disc_id = promo_disc_id;
		this.promo = promo;
		this.discount_value = discount_value;
	}

	public int getPromo_disc_id() {
		return promo_disc_id;
	}

	public void setPromo_disc_id(int promo_disc_id) {
		this.promo_disc_id = promo_disc_id;
	}

	public PromoEntity getPromo() {
		return promo;
	}

	public void setPromo(PromoEntity promo) {
		this.promo = promo;
	}

	public int getDiscount_value() {
		return discount_value;
	}

	public void setDiscount_value(int discount_value) {
		this.discount_value = discount_value;
	}

	@Override
	public String toString() {
		return "PromoDiscountEntity [promo_disc_id=" + promo_disc_id + ", promo=" + promo + ", discount_value="
				+ discount_value + "]";
	}
}
