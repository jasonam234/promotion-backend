package com.jason.promotion.promo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.jason.promotion.product.entity.ProductEntity;
import com.jason.promotion.product.repository.ProductCustomFields;
import com.jason.promotion.product.repository.ProductRepository;
import com.jason.promotion.promo.entity.PromoFreeGoodsEntity;
import com.jason.promotion.promo.repository.PromoCustomFields;
import com.jason.promotion.promo.repository.PromoDiscountCustomFields;
import com.jason.promotion.promo.repository.PromoFreeGoodsCustomFields;
import com.jason.promotion.promo.repository.PromoFreeGoodsRepository;
import com.jason.promotion.promo.repository.PromoRepository;
import com.jason.promotion.response.Response;
import com.jason.promotion.response.ResponseList;
import com.jason.promotion.response.ResponseMessage;
import com.jason.promotion.response.ResponseObject;

@Service
public class PromoFreeGoodsService {
	@Autowired
	PromoFreeGoodsRepository promoFreeGoodsRepository;
	
	@Autowired
	PromoRepository promoRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	public ResponseEntity<Response> postNewPromoFreeGoods(PromoFreeGoodsEntity payload){
		ProductCustomFields.getAllProductData productData = productRepository.getProduct(payload.getProduct_code());
		
		if(productData == null) {
			Response response = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), 
					"POST new promo terminated, product not found");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response);
		}

		promoRepository.postNewPromo(payload.getPromo().getUser().getUser_id()
				, payload.getPromo().getDocument_number(), payload.getPromo().getDescription()
				, payload.getPromo().getStart_date(), payload.getPromo().getEnd_date(), payload.getPromo().getPromo_value());
		
		PromoCustomFields.getAllPromoData promoData = promoRepository.getLastPromo();
		
		promoFreeGoodsRepository.postNewPromoFreeGoods(promoData.getPromotion_id(), 
				productData.getProduct_id(), payload.getQuantity());
		PromoFreeGoodsCustomFields.PostNewPromoFreeGoodsData data = promoFreeGoodsRepository.getLatestPromoFreeGoods();
		
		Response response = new ResponseObject(HttpStatus.OK.value(), "POST promo free goods data successful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> getFreeGoodsPromoByPromoId(int promotion_id){
		PromoFreeGoodsCustomFields.GetProductIdAndQuantity data = 
				promoFreeGoodsRepository.getFreeGoodsPromoByPromoId(promotion_id);
		Response response = new ResponseObject(HttpStatus.OK.value(), "GET promo free goods data successful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> putFreeGoodsPromo(PromoFreeGoodsEntity payload){
		ProductCustomFields.getAllProductData productData = productRepository.getProduct(payload.getProduct_code());
		
		if(productData == null) {
			Response response = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), 
					"PUT promo operation terminated, product not found");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response);
		}
		
		promoRepository.putPromo(payload.getPromo().getDocument_number(), payload.getPromo().getDescription(), 
				payload.getPromo().getStart_date(), payload.getPromo().getEnd_date(), payload.getPromo().getPromo_value(), 
				payload.getPromo().getPromotion_id());
		
		promoFreeGoodsRepository.putPromoFreeGoods(productData.getProduct_id(), payload.getQuantity(), 
				payload.getPromo().getPromotion_id());
		
		PromoFreeGoodsCustomFields.GetProductIdAndQuantity data = 
				promoFreeGoodsRepository.getFreeGoodsPromoByPromoId(payload.getPromo().getPromotion_id());
		Response response = new ResponseObject(HttpStatus.OK.value(), "PUT promo free goods data successful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
}
