package com.jason.promotion.promo.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jason.promotion.product.repository.ProductCustomFields;
import com.jason.promotion.promo.entity.PromoEntity;

@Transactional
public interface PromoRepository extends JpaRepository<PromoEntity, Integer>{
	@Query(value = "select promotion_id, document_number, description"
			+ ", start_date, end_date, promo_value, last_edit from promotion", nativeQuery = true)
	List<PromoCustomFields.getAllPromoData> getAllPromo();
	
	@Query(value = "select p.promotion_id, p.document_number, p.description, p.start_date, p.end_date, "
			+ "p.promo_value, pr.name, p.last_edit from promotion p "
			+ "join item_term it on it.promotion_id = p.promotion_id "
			+ "join product pr on pr.product_id = it.product_id "
			+ "where p.document_number like %:doc_num% and it.product_id like %:product_id% and p.deleted = 0", nativeQuery = true)
	List<PromoCustomFields.getAllPromoData> postSearchPromo(@Param("doc_num") String document_number, 
			@Param("product_id") String product_id);
	
	@Query(value = "select promotion_id, document_number from promotion", nativeQuery = true)
	List<PromoCustomFields.getDocumentNumber> getDocumentNumber();
	
	@Query(value = "select it.product_id from promotion p "
			+ "join item_term it on it.promotion_id = p.promotion_id "
			+ "join product pr on pr.product_id = it.product_id "
			+ "group by it.product_id", nativeQuery = true)
	List<PromoCustomFields.getProductId> getProductId();
	
	@Query(value = "select promotion_id, document_number, description"
			+ ", start_date, end_date, promo_value, last_edit from promotion where promotion_id = ?", nativeQuery = true)
	PromoCustomFields.getAllPromoData getPromoById(int promotion_id);
	
	@Query(value = "select promotion_id, document_number, description"
			+ ", start_date, end_date, promo_value, last_edit from promotion ORDER BY promotion_id DESC LIMIT 1", nativeQuery = true)
	PromoCustomFields.getAllPromoData getLastPromo();

	@Modifying
	@Query(value = "insert into promotion(user_id, document_number, description, start_date, "
			+ "end_date, promo_value, last_edit, deleted) "
			+ "value (?1, ?2, ?3, ?4, ?5, ?6, now(), 0)", nativeQuery = true)
	int postNewPromo(int user_id, String document_number, String description, String start_date, 
			String end_date, int promo_value);

	@Modifying
	@Query(value = "update promotion set "
			+ "document_number = ?1, description = ?2, start_date= ?3, end_date= ?4, promo_value= ?5, last_edit = now() "
			+ "where promotion_id= ?6", nativeQuery = true)
	int putPromo(String document_number, String description, String start_date, String end_date, int promo_value, int promotion_id);
	
	@Modifying
	@Query(value = "update promotion set deleted = 1 where promotion_id = ?", nativeQuery = true)
	int deletePromo(int promotion_id);

}
