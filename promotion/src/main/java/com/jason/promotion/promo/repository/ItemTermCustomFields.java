package com.jason.promotion.promo.repository;

public interface ItemTermCustomFields {
	public interface getAllItemTermData{
		public int getTerm_id();
		public int getPromotion_id();
		public int getProduct_id();
	}
	
	public interface getItemTermByPromoId{
		public int getTerm_id();
		public int getProduct_id();
		public String getName();
		public String getProduct_code();
	}
}
