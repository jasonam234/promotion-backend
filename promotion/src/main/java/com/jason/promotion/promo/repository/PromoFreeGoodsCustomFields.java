package com.jason.promotion.promo.repository;

public interface PromoFreeGoodsCustomFields {
	public interface PostNewPromoFreeGoodsData{
		public int getPromotion_id();
		public int getPromotion_free_goods_id();
		public String getDocument_number();
		public String getDescription();
		public String getStart_date();
		public String getEnd_date();
		public String getPromo_value();
		public String getName();
		public String getProduct_code();
		public String getLast_edit();
	}
	
	public interface GetProductIdAndQuantity{
		public int getPromotion_free_goods_id();
		public int getPromotion_id();
		public int getProduct_id();
		public String getName();
		public int getQuantity();
		public String getProduct_code();
	}
	
	public interface CheckFreeGoodsPromo{
		public int getPromotion_id();
		public int getQuantity();
		public int getProduct_id();
		public String getPromo_value();
	}
}
