package com.jason.promotion.promo.repository;

public interface PromoCustomFields {
	public interface getAllPromoData{
		public int getPromotion_id();
		public String getDocument_number();
		public String getDescription();
		public String getStart_date();
		public String getEnd_date();
		public String getPromo_value();
		public String getName();
		public String getLast_edit();
	}
	
	public interface postSearchPromo{
		public int getPromotion_id();
		public String getDocument_number();
		public String getDescription();
		public String getStart_date();
		public String getEnd_date();
		public String getPromo_value();
		public String getName();
		public String getLast_edit();
		public int getProduct_id();
	}
	
	public interface getDocumentNumber{
		public int getPromotion_id();
		public String getDocument_number();
	}
	
	public interface getProductId{
		public int getProduct_id();
	}
}
