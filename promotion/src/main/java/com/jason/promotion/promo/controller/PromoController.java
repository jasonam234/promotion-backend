package com.jason.promotion.promo.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jason.promotion.config.SecuredRestController;
import com.jason.promotion.promo.entity.ProductRecycler;
import com.jason.promotion.promo.entity.PromoDiscountEntity;
import com.jason.promotion.promo.entity.PromoEntity;
import com.jason.promotion.promo.repository.PromoRepository;
import com.jason.promotion.promo.service.PromoService;
import com.jason.promotion.response.Response;

@RestController
public class PromoController implements SecuredRestController{
	@Autowired
	PromoRepository promoRepository;
	
	@Autowired
	PromoService promoService;
	
	@GetMapping(value = "/api/promotion/get/promos", produces = "application/json")
	public ResponseEntity<Response> getAllPromo(){
		return promoService.getAllPromo();
	}
	
	@GetMapping(value = "/api/promotion/get/promos/{promotion_id}", produces = "application/json")
	public ResponseEntity<Response> getPromoById(@PathVariable int promotion_id){
		return promoService.getPromoById(promotion_id);
	}
	
	@PostMapping(value = "/api/promotion/post/applied_promos/free_goods", produces = "application/json")
	public ResponseEntity<Response> searchAppliedPromoFreeGoods(@RequestBody ArrayList<ProductRecycler> payload){
		return promoService.searchAppliedPromoFreeGoods(payload);
	}
	
	@PostMapping(value = "/api/promotion/post/applied_promos/discount", produces = "application/json")
	public ResponseEntity<Response> searchAppliedPromoDiscounts(@RequestBody ArrayList<ProductRecycler> payload){
		return promoService.searchAppliedPromoDiscount(payload);
	}
	
	@PostMapping(value = "/api/promotion/post/search/promo", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> postPromoSearch(@RequestBody PromoEntity payload){
		return promoService.postPromoSearch(payload);
	}
	
	@DeleteMapping(value = "/api/promotion/delete/promo/{promotion_id}", produces = "application/json")
	public ResponseEntity<Response> deletePromo(@PathVariable int promotion_id){
		return promoService.deletePromo(promotion_id);
	}
}
