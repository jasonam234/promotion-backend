package com.jason.promotion.promo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jason.promotion.config.SecuredRestController;
import com.jason.promotion.promo.entity.PromoDiscountEntity;
import com.jason.promotion.promo.entity.PromoFreeGoodsEntity;
import com.jason.promotion.promo.service.PromoFreeGoodsService;
import com.jason.promotion.response.Response;

@RestController
public class PromoFreeGoodsController implements SecuredRestController{
	@Autowired
	PromoFreeGoodsService promoFreeGoodsService;
	
	@PostMapping(value = "/api/promotion/post/free_goods", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> postNewFreeGoodsPromo(@RequestBody PromoFreeGoodsEntity payload){
		return promoFreeGoodsService.postNewPromoFreeGoods(payload);
	}
	
	@GetMapping(value = "/api/promotion/get/free_goods/{promotion_id}", produces = "application/json")
	public ResponseEntity<Response> getFreeGoodsPromoByPromoId(@PathVariable int promotion_id){
		return promoFreeGoodsService.getFreeGoodsPromoByPromoId(promotion_id);
	}
	
	@PutMapping(value = "/api/promotion/put/free_goods", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> putFreeGoodsPromo(@RequestBody PromoFreeGoodsEntity payload){
		return promoFreeGoodsService.putFreeGoodsPromo(payload);
	}
}
