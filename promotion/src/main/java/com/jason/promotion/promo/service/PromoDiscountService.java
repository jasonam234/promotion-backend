package com.jason.promotion.promo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.jason.promotion.promo.entity.PromoDiscountEntity;
import com.jason.promotion.promo.entity.PromoEntity;
import com.jason.promotion.promo.repository.PromoCustomFields;
import com.jason.promotion.promo.repository.PromoDiscountCustomFields;
import com.jason.promotion.promo.repository.PromoDiscountRepository;
import com.jason.promotion.promo.repository.PromoRepository;
import com.jason.promotion.response.Response;
import com.jason.promotion.response.ResponseList;
import com.jason.promotion.response.ResponseObject;

@Service
public class PromoDiscountService {
	@Autowired
	PromoDiscountRepository promoDiscountRepository;
	
	@Autowired
	PromoRepository promoRepository;
	
	public ResponseEntity<Response> getAllPromoDiscount(){
		List<PromoDiscountCustomFields.getAllPromoDiscountData> data = promoDiscountRepository.getAllPromoDiscount();
		Response response = new ResponseObject(HttpStatus.OK.value(), "GET all promo data successful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> getDiscountPromoByPromoId(int promotion_id){
		PromoDiscountCustomFields.getDiscountValue data =  
				promoDiscountRepository.getDiscountByPromoId(promotion_id);
		Response response = new ResponseObject(HttpStatus.OK.value(), "GET discount promo by promo id successful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> postNewDiscountPromo(PromoDiscountEntity payload){
		promoRepository.postNewPromo(payload.getPromo().getUser().getUser_id()
				, payload.getPromo().getDocument_number(), payload.getPromo().getDescription()
				, payload.getPromo().getStart_date(), payload.getPromo().getEnd_date(), payload.getPromo().getPromo_value());
		
		PromoCustomFields.getAllPromoData promoData = promoRepository.getLastPromo();
		
		promoDiscountRepository.postNewPromoDiscount(promoData.getPromotion_id(), payload.getDiscount_value());
		PromoDiscountCustomFields.getLatestPromo data = promoDiscountRepository.getLastPromoDiscount();
		
		Response response = new ResponseObject(HttpStatus.OK.value(), "POST promo discount data succesful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> putDiscountPromo(PromoDiscountEntity payload){
		promoRepository.putPromo(payload.getPromo().getDocument_number(), payload.getPromo().getDescription(), 
				payload.getPromo().getStart_date(), payload.getPromo().getEnd_date(), payload.getPromo().getPromo_value(), 
				payload.getPromo().getPromotion_id());
		
		promoDiscountRepository.putPromoDiscount(payload.getDiscount_value(), payload.getPromo().getPromotion_id());
		List<PromoDiscountCustomFields.getLatestPromo> data =  
				promoDiscountRepository.getDiscountByPromoIdComplete(payload.getPromo().getPromotion_id());
		Response response = new ResponseList(HttpStatus.OK.value(), "PUT discount promo by promo id successful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
}
