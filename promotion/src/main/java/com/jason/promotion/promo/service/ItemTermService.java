package com.jason.promotion.promo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.jason.promotion.promo.entity.ItemTermEntity;
import com.jason.promotion.promo.repository.ItemTermCustomFields;
import com.jason.promotion.promo.repository.ItemTermRepository;
import com.jason.promotion.response.Response;
import com.jason.promotion.response.ResponseList;
import com.jason.promotion.response.ResponseObject;

@Service
public class ItemTermService {
	@Autowired
	ItemTermRepository itemTermRepository;
	
	public ResponseEntity<Response> postNewTerm(ItemTermEntity payload){
		itemTermRepository.postNewItemTerm(payload.getPromo().getPromotion_id(), payload.getProduct().getProduct_id());
		ItemTermCustomFields.getAllItemTermData data = itemTermRepository.getLastTerm();
		Response response = new ResponseObject(HttpStatus.OK.value(), "POST new term successful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> getItemTermByPromoId(int promotion_id){
		List<ItemTermCustomFields.getItemTermByPromoId> data =  itemTermRepository.getItemTermByPromoId(promotion_id);
		Response response = new ResponseList(HttpStatus.OK.value(), "GET all term successful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> putItemTerm(ItemTermEntity payload){
		itemTermRepository.putItemTerm(payload.getProduct().getProduct_id(), payload.getTerm_id());
		ItemTermCustomFields.getItemTermByPromoId data = itemTermRepository.getItemTermById(payload.getTerm_id());
		Response response = new ResponseObject(HttpStatus.OK.value(), "PUT item term successful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
}
