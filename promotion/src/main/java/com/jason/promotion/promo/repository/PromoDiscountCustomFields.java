package com.jason.promotion.promo.repository;

import com.jason.promotion.promo.entity.PromoEntity;

public interface PromoDiscountCustomFields {
	public interface getAllPromoDiscountData{
		public int getPromo_disc_id();
		public int getPromotion_id();
		public int getDiscount_value();
	}
	
	public interface getLatestPromo{
		public int getPromotion_id();
		public int getDiscount_value();
		public String getDocument_number();
		public String getDescription();
		public String getStart_date();
		public String getEnd_date();
		public String getPromo_value();
		public String getLast_edit();
	}
	
	public interface getDiscountValue{
		public int getPromo_disc_id();
		public int getDiscount_value();
		public String getProduct_code();
	}
	
	public interface checkDiscountPromo{
		public int getPromotion_id();
		public int getPrice();
		public int getDiscount_value();
		public int getProduct_id();
		public String getPromo_value();
	}
}
