package com.jason.promotion.user.repository;

public interface UserCustomResponse {
	public interface getUsername{
		public int getUser_id();
		public String getUsername();
	}
}
