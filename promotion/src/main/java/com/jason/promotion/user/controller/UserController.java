package com.jason.promotion.user.controller;

import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jason.promotion.config.JwtTokenUtil;
import com.jason.promotion.config.SecuredRestController;
import com.jason.promotion.response.JwtRequest;
import com.jason.promotion.response.Response;
import com.jason.promotion.response.ResponseList;
import com.jason.promotion.response.ResponseMessage;
import com.jason.promotion.response.ResponseObject;
import com.jason.promotion.response.ResponseString;
import com.jason.promotion.user.entity.UserEntity;
import com.jason.promotion.user.repository.UserCustomResponse;
import com.jason.promotion.user.repository.UserRepository;
import com.jason.promotion.user.service.JwtUserDetailsService;

@RestController
public class UserController implements SecuredRestController{
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	@Autowired
	private PasswordEncoder bcryptEncoder;

	@RequestMapping(value = "/api/authenticate", method = RequestMethod.POST)
	public ResponseEntity<Response> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) {
		UserEntity dataUser = userRepository.findByUsername(authenticationRequest.getUsername());
		
		Boolean passwordChecker = bcryptEncoder.matches(authenticationRequest.getPassword(), dataUser.getPassword());
	
		if(passwordChecker == false || dataUser == null) {
			Response response = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Login gagal, username atau password salah");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response);
		}
		
		final String token = jwtTokenUtil.generateToken(dataUser);
		
		Response response = new ResponseString(HttpStatus.OK.value(), "Login Berhasil", token);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	@RequestMapping(value = "/api/register", method = RequestMethod.POST)
	public ResponseEntity<Response> saveUser(@RequestBody UserEntity user) throws Exception {
		List<UserEntity> usernameAvailable = userRepository.findUsername(user.getUsername());
		
		if(usernameAvailable.size() > 0) {
			Response response = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), "Username sudah terdaftar");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(response);
		}
		
		userDetailsService.save(user);
		UserEntity data = userRepository.getUserByUsername(user.getUsername());
		Response response = new ResponseObject(HttpStatus.OK.value(), "Pendaftaran pengguna berhasil", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
}
