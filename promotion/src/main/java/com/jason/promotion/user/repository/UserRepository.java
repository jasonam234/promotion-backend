package com.jason.promotion.user.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.jason.promotion.user.entity.UserEntity;

@Transactional
public interface UserRepository extends JpaRepository<UserEntity, Integer>{
	@Query(value = "SELECT username FROM user", nativeQuery = true)
	List<UserCustomResponse.getUsername> getAllUser();
	
	@Modifying
	@Query(value = "INSERT INTO user(username, password, last_edit, deleted) VALUE(?, ?, now(), 0)", nativeQuery = true)
	int registerUser(String username, String password);
	
	@Query(value = "SELECT * FROM user WHERE username= ?1", nativeQuery = true)
	List<UserEntity> findUsername(String username);
	
	@Query(value = "SELECT * FROM user WHERE username= ?1", nativeQuery = true)
	UserEntity getUserByUsername(String username);
	
	@Query(value = "SELECT username FROM user WHERE username = ?1 AND password = ?2", nativeQuery = true)
	UserCustomResponse.getUsername signinUser(String username, String password);
	
	UserEntity findByUsername(String username);
}
