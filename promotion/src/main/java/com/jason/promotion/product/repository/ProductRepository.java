package com.jason.promotion.product.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.jason.promotion.product.entity.ProductEntity;

@Transactional
public interface ProductRepository extends JpaRepository<ProductEntity, Integer>{
	@Query(value = "select product_id, name, price, product_code from product where product_code = ?", nativeQuery = true)
	ProductCustomFields.getAllProductData getProduct(String product_code);
	
	@Query(value = "select product_id, name, product_code, price from product where product_id = ?", nativeQuery = true)
	ProductCustomFields.getAllProductDataById getProductByProductId(int product_id);
	
	@Query(value = "select stock from product where product_id = ?", nativeQuery = true)
	ProductCustomFields.getProductQuantity getProductQuantity(int product_id);
	
	@Modifying
	@Query(value = "update product set stock = stock - ?1 where product_id = ?2", nativeQuery = true)
	int updateProductStock(int quantity, int product_id);
}
