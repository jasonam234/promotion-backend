package com.jason.promotion.product.entity;

import javax.persistence.*;

import com.jason.promotion.user.entity.UserEntity;

@Entity(name = "product")
@Table(name = "product")
public class ProductEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_id")
	int product_id;
	
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName= "user_id")
	UserEntity user;
	
	String name;
	int stock;
	int price;
	String product_code;
	String description;
	String image;
	String last_edit;
	
	public ProductEntity() { }

	public ProductEntity(int product_id, UserEntity user, String name, int stock, int price, String product_code,
			String description, String image, String last_edit) {
		super();
		this.product_id = product_id;
		this.user = user;
		this.name = name;
		this.stock = stock;
		this.price = price;
		this.product_code = product_code;
		this.description = description;
		this.image = image;
		this.last_edit = last_edit;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getLast_edit() {
		return last_edit;
	}

	public void setLast_edit(String last_edit) {
		this.last_edit = last_edit;
	}

	@Override
	public String toString() {
		return "ProductEntity [product_id=" + product_id + ", user=" + user + ", name=" + name + ", stock=" + stock
				+ ", price=" + price + ", product_code=" + product_code + ", description=" + description + ", image="
				+ image + ", last_edit=" + last_edit + "]";
	}
}
