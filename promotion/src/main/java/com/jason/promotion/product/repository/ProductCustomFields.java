package com.jason.promotion.product.repository;

public interface ProductCustomFields {
	public interface getAllProductData {
		public int getProduct_id();
		public String getProduct_code();
		public int getPrice();
		public String getName();
	}
	
	public interface getAllProductDataById {
		public int getProduct_id();
		public String getProduct_code();
		public String getName();
		public int getPrice();
	}
	
	public interface getProductQuantity{
		public int getStock();
	}
}
