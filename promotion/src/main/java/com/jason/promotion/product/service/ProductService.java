package com.jason.promotion.product.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.jason.promotion.product.repository.ProductCustomFields;
import com.jason.promotion.product.repository.ProductRepository;
import com.jason.promotion.response.Response;
import com.jason.promotion.response.ResponseMessage;
import com.jason.promotion.response.ResponseObject;

@Service
public class ProductService {
	@Autowired
	ProductRepository productRepository;
	
	public ResponseEntity<Response> getProductByCode(String product_code) {
		ProductCustomFields.getAllProductData data = productRepository.getProduct(product_code);
		
		if(data == null) {
			Response response = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), 
					"GET product data terminated, product not found");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response);
		}
		Response response = new ResponseObject(HttpStatus.OK.value(), "GET product data succesful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
	
	public ResponseEntity<Response> getProductByProductCodeAndQuantity(String product_code, int quantity) {
		ProductCustomFields.getAllProductData data = productRepository.getProduct(product_code);
		if(data == null) {
			Response response = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), 
					"P | GET product data terminated, product not found");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response);
		}
		
		ProductCustomFields.getProductQuantity productStock = productRepository.getProductQuantity(data.getProduct_id());
		if(quantity > productStock.getStock()) {
			Response response = new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), 
					"Q | GET product data terminated, insufficient stock");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(response);
		}
		
		Response response = new ResponseObject(HttpStatus.OK.value(), "GET product data succesful", data);
		return ResponseEntity.status(HttpStatus.OK.value()).body(response);
	}
}
