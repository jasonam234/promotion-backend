package com.jason.promotion.product.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.jason.promotion.config.SecuredRestController;
import com.jason.promotion.product.repository.ProductCustomFields;
import com.jason.promotion.product.repository.ProductRepository;
import com.jason.promotion.product.service.ProductService;
import com.jason.promotion.response.Response;
import com.jason.promotion.response.ResponseMessage;
import com.jason.promotion.response.ResponseObject;

@RestController
public class ProductController implements SecuredRestController{
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	ProductService productService;
	
	@GetMapping(value = "/api/product/{product_code}", produces = "application/json")
	public ResponseEntity<Response> getProductByProductCode(@PathVariable String product_code){
		return productService.getProductByCode(product_code);	
	}
	
	@GetMapping(value = "/api/product/{product_code}/{quantity}", produces = "application/json")
	public ResponseEntity<Response> getProductByProductCodeAndQuantity(@PathVariable String product_code, @PathVariable int quantity){
		return productService.getProductByProductCodeAndQuantity(product_code, quantity);	
	}
	
}
