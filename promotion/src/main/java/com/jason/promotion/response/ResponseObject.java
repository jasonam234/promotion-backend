package com.jason.promotion.response;

public class ResponseObject implements Response{
	int status;
	String message;
	Object data;
	
	public ResponseObject(int status, String message, Object data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "ResponseObject [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
}
