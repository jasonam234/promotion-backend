package com.jason.promotion.response;

import java.util.List;

public class ResponseList implements Response{
	int status;
	String message;
	List<?> data;
	
	public ResponseList(int status, String message, List<?> data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "ResponseList [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
}
